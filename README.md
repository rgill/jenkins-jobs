# Jenkins jobs configs

## Test your local jjb changes against a target jenkins instance

```sh
sudo docker run --dns=10.0.2.15 -e JENKINS_URL=http://jenkins-alm.router.default.svc.cluster.local -v "$(pwd)":/opt/src -it pp2l/jenkins-configure
```
